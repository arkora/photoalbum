module org.company {
    requires javafx.controls;
    requires javafx.fxml;
    requires metadata.extractor;
    requires java.persistence;
    requires org.hibernate.orm.core;
    requires java.naming;
    requires org.postgresql.jdbc;
    requires com.dlsc.gmapsfx;
    requires javafx.swing;


    opens org.company to javafx.fxml;
    opens org.company.ui to javafx.fxml;

    exports org.company;
}