package org.company.service;


import com.drew.imaging.ImageProcessingException;
import org.company.metadata.PhotoMetadataBuilder;
import org.company.model.Photo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class PhotoFileService {



    public static Photo getPhotoFromFile(File file)throws IOException, ImageProcessingException{
        Photo photo = new Photo();
        photo.setPhoto(Files.readAllBytes(file.toPath()));
        photo.setMetadata(PhotoMetadataBuilder.build(file));
        return photo;
    }


}
