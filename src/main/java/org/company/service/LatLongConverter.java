package org.company.service;

import com.dlsc.gmapsfx.javascript.object.LatLong;
import org.company.model.Photo;

import java.util.ArrayList;
import java.util.List;

public class LatLongConverter {

    public static List<LatLong> toLatLong(List<Photo> photoList){
        List<LatLong> latLongList = new ArrayList<>();
        for (Photo photo : photoList){
            double lat = Double.parseDouble(photo.getLatitude());
            double lon = Double.parseDouble(photo.getLongitude());
            latLongList.add(new LatLong(lat,lon));

        }
        return latLongList;
    }
}
