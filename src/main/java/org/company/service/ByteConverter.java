package org.company.service;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class ByteConverter {

    public static Image byteToImage(byte[] photo){
        return new Image(new ByteArrayInputStream(photo));

    }

    public static byte[] imageToBYte(Image image) throws IOException {
       BufferedImage bImage = SwingFXUtils.fromFXImage(image,null);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();



        ImageIO.write(bImage, "jpg",outputStream);

        byte[] res  = outputStream.toByteArray();
        outputStream.close();





        return res;

    }

}
