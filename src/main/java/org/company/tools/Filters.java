package org.company.tools;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;

public enum Filters {
    GRAYSCALE(0.2162,0.7152,0.0722),SEPIA(112,66,20),NEGATIVE(255,255,255);

    public final double red;
    public final double green;
    public final double blue;

    Filters(double red, double green, double blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public double getRed() {
        return red;
    }

    public double getGreen() {
        return green;
    }

    public double getBlue() {
        return blue;
    }

    //    public static Image grayscale(Image image) {
//        int width = (int) image.getWidth();
//        int height = (int) image.getHeight();
//        PixelReader pixelReader = image.getPixelReader();
//
//        WritableImage writableImage = new WritableImage(width, height);
//
//        for (int x = 0; x < width; x++) {
//            for (int y = 0; y < height; y++) {
//                int pixel = pixelReader.getArgb(x, y);
//
//                int alpha = ((pixel >> 24) & 0xff);
//                int red = ((pixel >> 16) & 0xff);
//                int green = ((pixel >> 8) & 0xff);
//                int blue = (pixel & 0xff);
//
//                int grayLevel = (int) (0.2162 * red + 0.7152 * green + 0.0722 * blue);
//                int gray = (alpha << 24) + (grayLevel << 16) + (grayLevel << 8) + grayLevel;
//
//                writableImage.getPixelWriter().setArgb(x, y, gray);
//
//
//            }
//        }
//        return writableImage;
//
//    }
//
//    public static Image sepia(Image image) {
//        int width = (int) image.getWidth();
//        int height = (int) image.getHeight();
//        PixelReader pixelReader = image.getPixelReader();
//
//        WritableImage writableImage = new WritableImage(width, height);
//
//        for (int x = 0; x < width; x++) {
//            for (int y = 0; y < height; y++) {
//                int pixel = pixelReader.getArgb(x, y);
//
//
//
//                int alpha = ((pixel >> 24) & 0xff);
//                int red = ((pixel >> 16) & 0xff);
//                int green = ((pixel >> 8) & 0xff);
//                int blue = (pixel & 0xff);
//
//
//                int newRed = (int)(0.393*red + 0.769*green + 0.189*blue);
//                int newGreen = (int)(0.349*red + 0.686*green + 0.168*blue);
//                int newBlue = (int)(0.272*red + 0.534*green + 0.131*blue);
//                int sepia = (alpha << 24) + (newRed << 16) + (newGreen << 8) + newBlue;
//
//                writableImage.getPixelWriter().setArgb(x, y, sepia);
//
//            }
//        }
//        return writableImage;
//
//    }
//
//    public static Image negative(Image image) {
//        int width = (int) image.getWidth();
//        int height = (int) image.getHeight();
//        PixelReader pixelReader = image.getPixelReader();
//
//        WritableImage writableImage = new WritableImage(width, height);
//
//        for (int x = 0; x < width; x++) {
//            for (int y = 0; y < height; y++) {
//                int pixel = pixelReader.getArgb(x, y);
//
//
//
//                int alpha = ((pixel >> 24) & 0xff);
//                int red = ((pixel >> 16) & 0xff);
//                int green = ((pixel >> 8) & 0xff);
//                int blue = (pixel & 0xff);
//
//
//                int newRed = (int)(255-red);
//                int newGreen = (int)(255-green);
//                int newBlue = (int)(255-blue);
//                int negative = (alpha << 24) + (newRed << 16) + (newGreen << 8) + newBlue;
//
//                writableImage.getPixelWriter().setArgb(x, y, negative);
//
//            }
//        }
//        return writableImage;
//
//    }

}
