package org.company.tools;

import javafx.scene.image.Image;

import java.util.LinkedList;

public class ImageCondition {

  private LinkedList<Image> images = new LinkedList<>();
  private int index;

    public LinkedList<Image> getImages() {
        return images;
    }



    public void addImage(Image image){
        images.add(image);
        index = images.size()-1;
    }

    public Image undo() {
            if (index>0){
                index--;
                return images.get(index);
            }else {return images.get(0);}


    }

    public Image redo(){
        if (index<=(images.size()-1)){
            return images.get(index++);
        }else {return images.getLast();}
    }

    public Image lastChange(){
        return images.getLast();
    }
}
