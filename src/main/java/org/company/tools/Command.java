package org.company.tools;

import java.util.Optional;

public interface Command {

    Optional<Filters> undo();
    Optional<Filters> redo();
}
