package org.company.tools;


import java.util.Optional;
import java.util.Stack;

public class ImageState implements Command{


    Stack<Filters> undoStack = new Stack<>();
    Stack<Filters> redoStack = new Stack<>();





    public void addInUndoStack(Filters filters){
        undoStack.push(filters);
    }

    @Override
    public Optional<Filters> undo() {
        if (!undoStack.isEmpty()){
            Filters filters = undoStack.pop();
            redoStack.push(filters);
            return Optional.of(filters);
        }
        return Optional.empty();
    }

    @Override
    public  Optional<Filters> redo() {
        if (!redoStack.isEmpty())
        {

           Filters filters = redoStack.peek();
            redoStack.pop();
            undoStack.push(filters);
            return Optional.of(filters);
        }
        return Optional.of(undoStack.peek());

    }


}
