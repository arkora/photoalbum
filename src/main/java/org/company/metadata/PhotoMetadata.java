package org.company.metadata;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.GpsDescriptor;
import com.drew.metadata.exif.GpsDirectory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;


public class PhotoMetadata {
    private String name;
    private String size;
    private String latitude;
    private String longitude;
    private String creationTime;

    PhotoMetadata(String name, String size, String latitude, String longitude, String creationTime) {
        this.name = name;
        this.size = size;
        this.latitude = latitude;
        this.longitude = longitude;
        this.creationTime = creationTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getLatidudeMetadata(File file) {

        try {
            Metadata metadata = ImageMetadataReader.readMetadata(file);
            if (metadata.containsDirectoryOfType(GpsDirectory.class)) {
                GpsDirectory gpsDir = metadata.getFirstDirectoryOfType(GpsDirectory.class);
                GpsDescriptor gpsDesc = new GpsDescriptor(gpsDir);
                latitude = gpsDesc.getGpsLatitudeDescription();


            }
        } catch (ImageProcessingException ex) {

            return ex.toString();
        } catch (IOException ex) {

            return ex.toString();
        }
        if (latitude == null)
            return "Not Found";
        else
            return latitude;

    }

    public String getLongitudeMetadata(File file) {

        try {
            Metadata metadata = ImageMetadataReader.readMetadata(file);
            if (metadata.containsDirectoryOfType(GpsDirectory.class)) {
                GpsDirectory gpsDir = metadata.getFirstDirectoryOfType(GpsDirectory.class);
                GpsDescriptor gpsDesc = new GpsDescriptor(gpsDir);
                longitude = gpsDesc.getGpsLongitudeDescription();


            }
        } catch (ImageProcessingException ex) {
            return ex.toString();
        } catch (IOException ex) {
            return ex.toString();
        }
        if (longitude == null)
            return "Not Found";
        else
            return longitude;

    }

    public String getSizeMetadata(File file) {
        try {
            Path path = Paths.get(String.valueOf(file));
            long sizeBytes = Files.size(path) / 1024;
            size = String.valueOf(sizeBytes);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return size;
    }

    public String getNameMetadata(File file) {

        Path path = Paths.get(String.valueOf(file));
        name = path.getFileName().toString();


        return name;
    }

    public String getCreationtTimeMetadata(File file) {
     try {
         Metadata metadata = ImageMetadataReader.readMetadata(file);
         if (metadata.containsDirectoryOfType(ExifSubIFDDirectory.class)) {
             ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
             Date date = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
             creationTime = date.toString();
         }
     }
      catch (ImageProcessingException ex) {
         return ex.toString();
     } catch (IOException ex) {
         return ex.toString();
     }
        if (creationTime == null)
            return "Not Found";
        else
            return creationTime;
        }
    }




