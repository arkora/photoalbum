package org.company.metadata;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.GpsDescriptor;
import com.drew.metadata.exif.GpsDirectory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

public class PhotoMetadataBuilder {

    public static PhotoMetadata build(File file) throws IOException, ImageProcessingException {
        Path path = Paths.get(String.valueOf(file));
        String name = path.getFileName().toString();

        long sizeBytes = Files.size(path) / 1024;

        String size = String.valueOf(sizeBytes);
        String longitude = "Not Found";
        String latitude = "Not Found";
        String creationTime = "Not Found";

        Metadata metadata = ImageMetadataReader.readMetadata(file);
        if (metadata.containsDirectoryOfType(GpsDirectory.class)) {
            GpsDirectory gpsDir = metadata.getFirstDirectoryOfType(GpsDirectory.class);
            GpsDescriptor gpsDesc = new GpsDescriptor(gpsDir);

            if (!gpsDesc.getGpsLatitudeDescription().equals(null)) {
                latitude = gpsDesc.getGpsLatitudeDescription();
                longitude = gpsDesc.getGpsLongitudeDescription();
            }
        }
        if(metadata.containsDirectoryOfType(ExifSubIFDDirectory.class)){
            ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
            Date date = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);

            if (!directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL).equals(null)){
                creationTime = date.toString();
            }
        }
        return new PhotoMetadata(name, size, latitude, longitude, creationTime);
    }


}
