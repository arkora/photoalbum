package org.company.ui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.company.controller.ImageGalleryController;
import org.company.model.Photo;


import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ImageGallery implements Initializable {

    @FXML
    Button btnNext;

    @FXML
    AnchorPane anchorPane;

    @FXML
    Button btnPrev;

    @FXML
    Button btnLoad;

    @FXML
    Button btnImageEditor;

    @FXML
    Button btnMap;

    ImageGalleryController imageGalleryController = new ImageGalleryController();

    @FXML
    ImageView imgView;

    int albumId;

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
            btnNext.setDisable(true);
            btnPrev.setDisable(true);
            btnImageEditor.setDisable(true);
            btnMap.setDisable(true);


    }


    @FXML
    public void next(){
        imgView.setImage(imageGalleryController.next());
    }

    @FXML
    public void previous(){
        imgView.setImage(imageGalleryController.previous());
    }

   @FXML
    public void load(){
        imageGalleryController.initPhotoList(albumId);
        imgView.setImage(imageGalleryController.getFirstImage());
        btnLoad.setDisable(true);
       btnNext.setDisable(false);
       btnPrev.setDisable(false);
       btnImageEditor.setDisable(false);
       btnMap.setDisable(false);
   }

   @FXML
    public void openImageEditor(){
       try {
           //Load second scene
           FXMLLoader loader =new FXMLLoader(getClass().getResource("/imageEditor.fxml"));
           Parent root = loader.load();


           int index = imageGalleryController.getIndex();

           List<Photo> photoList = imageGalleryController.getPhotoList();
           int photoId = photoList.get(index).getPhotoId();

           //Pass Value
           ImageEditor imageEditor = loader.getController();
           imageEditor.setPhotoId(photoId);


           Stage stage = new Stage();
           stage.setScene(new Scene(root));

           stage.show();
       } catch (IOException ex) {
           System.err.println(ex);
       }
   }

   @FXML void openMap(){
       try {
           //Load second scene
           FXMLLoader loader =new FXMLLoader(getClass().getResource("/map.fxml"));
           Parent root = loader.load();


           //Pass Value
            Map map = loader.getController();
            map.setPhotoList(imageGalleryController.getPhotoList());


           Stage stage = new Stage();
           stage.setScene(new Scene(root));

           stage.show();
       } catch (IOException ex) {
           System.err.println(ex);
       }

   }





}
