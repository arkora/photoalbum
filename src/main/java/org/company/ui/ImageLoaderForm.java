package org.company.ui;


import com.drew.imaging.ImageProcessingException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import org.company.controller.PhotoController;
import org.company.metadata.PhotoMetadata;
import org.company.model.Photo;
import org.company.repository.AlbumRepository;
import org.company.repository.PhotoRepository;
import org.company.service.ByteConverter;
import org.company.service.PhotoFileService;
import org.company.util.HibernateUtil;
import org.hibernate.Session;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class ImageLoaderForm  implements Initializable {



    FileChooser fileChooser = new FileChooser();
    File file ;
    PhotoController photoController = new PhotoController();


    @FXML
    Button loadImgBtn;
    @FXML
    Button saveImgBtn;
    @FXML
    ImageView imgView;
    @FXML
    TextField imageNameText;
    @FXML
    Label nameLabel;
    @FXML
    Label sizeLabel;
    @FXML
    Label sizeValueLabel;
    @FXML
    Label latitudeLabel;
    @FXML
    Label longitudeLabel;
    @FXML
    Label latitudeValueLabel;
    @FXML
    Label longitudeValueLabel;

    @FXML
    public void loadImage(){


        file = fileChooser.showOpenDialog(null);

        try {

              photoController.openPhoto(file);
        }catch (IOException | ImageProcessingException e) {

            Alert noFileAlert = new Alert(Alert.AlertType.ERROR);
            noFileAlert.setTitle("Error file in not jpg type");
            noFileAlert.setHeaderText("Wrong file type");
            noFileAlert.setContentText("Press OK to open Photo");

            Optional<ButtonType> result = noFileAlert.showAndWait();
            if (result.get() == ButtonType.OK)
                loadImage();
        }

              imgView.setImage(ByteConverter.byteToImage(photoController.getPhoto().getPhoto()));
              latitudeValueLabel.setText(photoController.getMetadata().getLatitude());
              longitudeValueLabel.setText(photoController.getMetadata().getLongitude());
              sizeValueLabel.setText(photoController.getMetadata().getSize());
              imageNameText.setText(photoController.getMetadata().getName());
              loadImgBtn.setDisable(true);
              saveImgBtn.setDisable(false);

    }


    @FXML
    public void saveMetadata(){

        if (file == null) {
            Alert noFileAlert = new Alert(Alert.AlertType.ERROR);
            noFileAlert.setTitle("Error no file found");
            noFileAlert.setHeaderText("No Photo Selected");
            noFileAlert.setContentText("Press OK to open Photo");

            Optional<ButtonType> result = noFileAlert.showAndWait();
            if (result.get() == ButtonType.OK)
                loadImage();
        }
        // Database photo insertion
        photoController.savePhoto(imageNameText.getText());
        loadImgBtn.setDisable(false);
        saveImgBtn.setDisable(true);

    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        saveImgBtn.setDisable(true);
    }
}
