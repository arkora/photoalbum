package org.company.ui;


import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import org.company.controller.ViewAllPhotosController;
import org.company.model.Photo;
import org.company.service.ByteConverter;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ViewAllPhotos implements Initializable {

    @FXML
    TilePane tileP;


    private ViewAllPhotosController viewAllPhotosController = new ViewAllPhotosController();

    private static final double ELEMENT_SIZE = 150;
    private static final double GAP = ELEMENT_SIZE / 10;




    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        createElements();
    }

    private void createElements() {

        List<VBox> vBoxList = createPage(viewAllPhotosController.loadPhotos());


        for (int i = 0; i<vBoxList.size(); i++){
            tileP.getChildren().add(vBoxList.get(i));
            tileP.setHgap(GAP);
            tileP.setVgap(GAP);
        }
    }


    public List<VBox>  createPage(List<Photo> photoList) {
        List<VBox> vBoxList = new ArrayList<>();

        for (Photo photo : photoList){
            VBox pageBox = createVBox(photo);
            vBoxList.add(pageBox);
        }

        return vBoxList;
    }

    private VBox createVBox(Photo photo) {
        Image image = ByteConverter.byteToImage(photo.getPhoto());

        ImageView imageView = new ImageView();
        imageView.setImage(image);
        imageView.setFitWidth(ELEMENT_SIZE);
        imageView.setFitHeight(ELEMENT_SIZE);

        imageView.setSmooth(true);
        imageView.setCache(true);

        Label nameLabel = new Label("Name: " + photo.getName());
        Label sizeLabel = new Label("Size: " + photo.getSize());
        Label longitudeLabel = new Label("Longitude: " + photo.getLongitude());
        Label latitudeLabel = new Label("Latitude: " + photo.getLatitude());


        VBox pageBox = new VBox();
        pageBox.getChildren().add(imageView);
        pageBox.getChildren().add(nameLabel);
        pageBox.getChildren().add(sizeLabel);
        pageBox.getChildren().add(latitudeLabel);
        pageBox.getChildren().add(longitudeLabel);
        pageBox.setSpacing(10);
        return pageBox;
    }

}
