package org.company.ui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import org.company.controller.ImageEditorController;
import org.company.repository.PhotoRepository;

import java.io.IOException;

public class ImageEditor {

    ImageEditorController imageEditorController = new ImageEditorController();


    @FXML
    Button btnSave;

    @FXML
    Button btnSepia;

    @FXML
    Button btnNegative;

    @FXML
    Button btnUndo;

    @FXML
    Button btnRedo;

    @FXML
    Button btnGrayscale;

    @FXML
    ImageView imgView;

    int photoId;
    //int count = 0;

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    @FXML
    public void saveImage() throws IOException {
        try {
            imageEditorController.savePhoto();

        }catch (IOException e){
            e.printStackTrace();
        }

    }

    @FXML
    public void grayscale(){
        imgView.setImage(imageEditorController.grayscale());

    }
    @FXML
    public void sepia(){
        imgView.setImage(imageEditorController.sepia());

    }

    @FXML
    public void negative(){
        imgView.setImage(imageEditorController.negative());

    }

    @FXML
    public void undo(){
        imgView.setImage(imageEditorController.undo());
    }

    @FXML
    public void redo(){
        imgView.setImage(imageEditorController.redo());
    }

    @FXML
    public void loadImage(){
        imageEditorController.loadPhoto(photoId);
        imgView.setImage(imageEditorController.getImage());
    }

    @FXML
    public void rotateImage(){
        imgView.setRotate(imgView.getRotate() + 90.0);
//        if (count == 0){
//            imgView.setRotate(90);
//            count = 1;
//        }
//        else if (count == 1){
//            imgView.setRotate(180);
//            count = 2;
//        }
//        else if (count == 2){
//            imgView.setRotate(270);
//            count = 3;
//        }
//        else if (count == 3){
//            imgView.setRotate(360);
//            count = 0;
//        }
    }


}
