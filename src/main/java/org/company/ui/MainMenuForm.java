package org.company.ui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;


import java.io.IOException;

public class MainMenuForm {

    @FXML
    Button createAlbumBtn;

    @FXML
    Button photoEditorBtn;

    @FXML
    Button allPhotoBtn;


    @FXML
    public void createAlbum() throws IOException{

               Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("createAlbumForm.fxml"));

                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.show();
    }

    @FXML
    public void imageEditor() throws IOException{

        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("imageEditor.fxml"));

        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    public void loadAlbums() throws IOException{
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("albumViewForm.fxml"));

        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    public void showAllPhotos() throws IOException{
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("viewAllPhotos.fxml"));

        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

}
