package org.company.ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.company.controller.AlbumViewFormController;
import org.company.controller.ImageGalleryController;
import org.company.model.Album;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class AlbumViewForm implements Initializable {
    @FXML
    private TilePane tilePane;

    AlbumViewFormController albumViewFormController = new AlbumViewFormController();

    private static final double ELEMENT_SIZE = 110;
    private static final double GAP = ELEMENT_SIZE / 10;

    int index;




    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        createElements();
    }

    private void createElements() {

        List<VBox> vBoxList = createPage(albumViewFormController.loadAlbums());


        for (int i = 0; i<vBoxList.size(); i++){
            tilePane.getChildren().add(vBoxList.get(i));
            tilePane.setHgap(GAP);
            tilePane.setVgap(GAP);
        }
    }


    public List<VBox>  createPage(List<Album> albumList) {
        List<VBox> vBoxList = new ArrayList<>();


        for (Album album : albumList){
            Image image = albumViewFormController.getFirstPhotoOfAlbum(album.getAlbumId());
            ImageView imageView = new ImageView();
            imageView.setImage(image);
            imageView.setFitWidth(ELEMENT_SIZE);
            imageView.setFitHeight(ELEMENT_SIZE);
            // imageView.setPreserveRatio(true);

            imageView.setSmooth(true);
            imageView.setCache(true);

            Label descLabel = new Label();
            descLabel.setText("Description: " + album.getDescription());
            Label locationLabel = new Label();
            locationLabel.setText("Location: " + album.getLocation());
            Button button = new Button();
            button.setText("Open Gallery");
            button.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {

                   loadImageGallery(album.getAlbumId());


                }
            });


            VBox pageBox = new VBox();
            pageBox.getChildren().add(imageView);
            pageBox.getChildren().add(descLabel);
            pageBox.getChildren().add(locationLabel);
            pageBox.getChildren().add(button);
            //pageBox.setStyle("-fx-border-color: black;");
            pageBox.setStyle("-fx-border-width: 2px");
            pageBox.setSpacing(10);
            vBoxList.add(pageBox);

        }

        return vBoxList;
    }

    public void loadImageGallery(int albumId){

        try {
            //Load second scene
            FXMLLoader loader =new FXMLLoader(getClass().getResource("/imageGallery.fxml"));
            Parent root = loader.load();

            //Pass Value
            ImageGallery imageGallery = loader.getController();
            imageGallery.setAlbumId(albumId);


            Stage stage = new Stage();
            stage.setScene(new Scene(root));

            stage.show();
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }




}
