package org.company.ui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.company.controller.AlbumController;
import org.company.model.Album;
import org.company.repository.AlbumRepository;
import org.company.util.HibernateUtil;
import org.hibernate.Session;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CreateAlbumForm implements Initializable {
    AlbumController albumController = new AlbumController();
    @FXML
    Button createAlbumBtn;

    @FXML
    Button addPhotoBtn;

    @FXML
    TextField descriptionText;

    @FXML
    TextField locationText;

    public void createAlbum(){

        albumController.saveAlbum(new Album(descriptionText.getText(),locationText.getText()));
        createAlbumBtn.setDisable(true);
        addPhotoBtn.setDisable(false);

    }

    @FXML
    public void addPhoto() throws IOException {

        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("imageLoaderForm.fxml"));


        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        addPhotoBtn.setDisable(true);
    }
}
