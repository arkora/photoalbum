package org.company.ui;

import com.dlsc.gmapsfx.GoogleMapView;
import com.dlsc.gmapsfx.javascript.event.GMapMouseEvent;
import com.dlsc.gmapsfx.javascript.event.UIEventType;
import com.dlsc.gmapsfx.javascript.object.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import org.company.controller.MapController;
import org.company.model.Photo;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ResourceBundle;

public class Map implements Initializable {


    @FXML
    private GoogleMapView googleMapView;

    private GoogleMap map;

    List<Photo> photoList ;

    public void setPhotoList(List<Photo> photoList) {
        this.photoList = photoList;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MapController mapController = new MapController();
        googleMapView.addMapInitializedListener(() -> mapController.configureMap(map,googleMapView));

    }



    protected void configureMap() {


        MapOptions mapOptions = new MapOptions();

        mapOptions.center(new LatLong(0, 0))
                .mapType(MapTypeIdEnum.HYBRID)
                .zoom(0);
        map = googleMapView.createMap(mapOptions, false);





    }
}
