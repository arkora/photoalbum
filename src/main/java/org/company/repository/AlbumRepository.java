package org.company.repository;


import org.company.model.Album;
import org.company.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

import javax.persistence.criteria.CriteriaBuilder;
import java.math.BigInteger;
import java.util.List;


public class AlbumRepository  {

    public static void saveAlbum (Album album){

        try (Session session = HibernateUtil.getSessionFactory().openSession() ) {
            session.beginTransaction();
            session.save(album);
            session.getTransaction().commit();

        } catch (HibernateException e){
            e.printStackTrace();
        }


    }

    public static BigInteger getLastInsertedAlbumId(){

        try(Session session = HibernateUtil.getSessionFactory().openSession()){
            return (BigInteger)  session.createSQLQuery("select last_value from albumid_seq").uniqueResult();
        }catch (HibernateException e){
            e.printStackTrace();
        }
        return null;
    }
    public static List<Album> getAllAlbums(){
        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createCriteria(Album.class).list();
        }catch (HibernateException e){
            e.printStackTrace();
        }
        return null;
    }


}
