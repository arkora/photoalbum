package org.company.repository;

import org.company.model.Photo;
import org.company.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;


public class PhotoRepository  {




    public static Photo getPhotoById(int photoid){
        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            return  session.get(Photo.class,photoid);

        }catch (HibernateException e){
            e.printStackTrace();
        }
        return null;


    }

    public static List<Photo> getAllPhotos(){
        try( Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createCriteria(Photo.class).list();
        }catch (HibernateException e){
            e.printStackTrace();
        }
        return null;
    }

   public static List<Photo> getPhotosByAlbumId(int albumid){

        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            String hql = ("FROM Photo E WHERE E.albumId = :id");
       Query query = session.createQuery(hql);
       query.setParameter("id", albumid);
       return query.list();
       //return results;
        }catch (HibernateException e){
            e.printStackTrace();
        }
        return null;



   }


    public static void setAlbumId(Photo photo,int id){

        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            Photo photoUpdate = (Photo) session.get(Photo.class,photo.getPhotoId());
            photoUpdate.setAlbumId(id);

            session.getTransaction().commit();
        }catch (HibernateException e){
            e.printStackTrace();
        }

    }

    public static void savePhoto(Photo photo) {


        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(photo);
            session.getTransaction().commit();
        }catch (HibernateException e){
            e.printStackTrace();
        }
    }




}
