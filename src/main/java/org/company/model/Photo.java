package org.company.model;

import org.company.metadata.PhotoMetadata;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "photos")
public class Photo implements Serializable {
    @Transient
    private PhotoMetadata metadata;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photoid")
    private int photoId;
    @Column(name = "name")
    private String name;
    @Column(name = "size")
    private String size;
    @Column(name = "photo")
    private byte[] photo;
    @Column(name = "latitude")
    private String latitude;
    @Column(name = "longitude")
    private String longitude;
    @Column(name = "albumid")
    private int albumId;
//    @Column(name = "creationtime")
//    private String creationTime;

    public Photo() {

    }


    public Photo(String name, String size, byte[] photo, String latitude, String longitude, int albumId) {
        this.name = name;
        this.size = size;
        this.photo = photo;
        this.latitude = latitude;
        this.longitude = longitude;
        this.albumId = albumId;
        //this.creationTime = creationTime;
    }

    public Photo(String name, String size, byte[] photo, String latitude, String longitude) {
        this.name = name;
        this.size = size;
        this.photo = photo;
        this.latitude = latitude;
        this.longitude = longitude;
        //this.creationTime = creationTime;
    }

    public PhotoMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(PhotoMetadata metadata) {
        this.metadata = metadata;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }
//    public String getCreationTime() {
//        return creationTime;
//    }
//
//    public void setCreationTime(String creationTime) {
//        this.creationTime = creationTime;
//    }
}
