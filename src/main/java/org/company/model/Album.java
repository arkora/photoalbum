package org.company.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "albums")
public class Album implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "albumid")
    private int albumId;
    @Column(name = "description")
    private String description;
    @Column(name = "location")
    private String location;

    public Album() {
    }

    public Album(int albumId, String description, String location) {
        this.albumId = albumId;
        this.description = description;
        this.location = location;
    }

    public Album(String description, String location) {
        this.description = description;
        this.location = location;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
