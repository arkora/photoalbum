package org.company.controller;


import javafx.scene.image.Image;

import org.company.model.Album;
import org.company.model.Photo;
import org.company.repository.AlbumRepository;
import org.company.repository.PhotoRepository;
import org.company.service.ByteConverter;

import java.util.List;

public class AlbumViewFormController {
    public List<Album> loadAlbums(){
        return AlbumRepository.getAllAlbums();
    }

    public Image getFirstPhotoOfAlbum(int albumId){
        List<Photo> photoList = PhotoRepository.getPhotosByAlbumId(albumId);
        return ByteConverter.byteToImage(photoList.get(0).getPhoto());
    }

}
