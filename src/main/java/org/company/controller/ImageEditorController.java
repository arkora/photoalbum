package org.company.controller;

import javafx.scene.image.Image;
import org.company.model.Photo;
import org.company.repository.PhotoRepository;
import org.company.service.ByteConverter;
import org.company.tools.FilterAppliance;

import org.company.tools.Filters;
import org.company.tools.ImageState;

import java.io.IOException;
import java.util.Optional;

public class ImageEditorController {

    ImageState imageState = new ImageState();

   private Photo photo ;
   private Image image,editedImage;

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public Image getImage() {
        return image;
    }

    public Image grayscale() {
        imageState.addInUndoStack(Filters.GRAYSCALE);
        return editedImage =  FilterAppliance.apply(this.image,Filters.GRAYSCALE);


    }

    public Image sepia(){
        imageState.addInUndoStack(Filters.SEPIA);
        return editedImage =  FilterAppliance.apply(this.image,Filters.SEPIA);
    }

    public Image negative(){
        imageState.addInUndoStack(Filters.NEGATIVE);
        return editedImage =  FilterAppliance.apply(this.image,Filters.NEGATIVE);


    }

    public Image undo(){
        Optional<Filters> undoFilter = imageState.undo();

        if (undoFilter.isPresent()) {
            Filters filters = undoFilter.get();
            return editedImage = FilterAppliance.apply(this.image,filters);
        }
        return editedImage = this.image;

    }

    public Image redo(){
        Optional<Filters> redoFilter = imageState.redo();
        Filters filters = redoFilter.get();
        return editedImage = FilterAppliance.apply(this.image,filters);
    }

    public void loadPhoto(int photoId){
        photo = PhotoRepository.getPhotoById(photoId);
        image = ByteConverter.byteToImage(photo.getPhoto());
        editedImage = this.image;


    }

    public void savePhoto() throws IOException {

        try {
            byte[] pic = ByteConverter.imageToBYte(this.editedImage);
            Photo photo1  = new Photo("name","size",pic,"1","1",2);
            PhotoRepository.savePhoto(photo1);



        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
