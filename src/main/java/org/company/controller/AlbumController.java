package org.company.controller;

import org.company.model.Album;
import org.company.repository.AlbumRepository;
import org.company.ui.CreateAlbumForm;

public class AlbumController {


    public void saveAlbum(Album album){
        AlbumRepository.saveAlbum(album);
    }
}
