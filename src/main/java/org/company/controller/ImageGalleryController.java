package org.company.controller;

import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import org.company.model.Photo;
import org.company.repository.PhotoRepository;
import org.company.service.ByteConverter;


import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ImageGalleryController implements Initializable {
    private List<Photo> photoList = new ArrayList<>();
    private int index = 0;
    private List<Image> imageList = new ArrayList<>();

    public List<Image> getImageList() {
        return imageList;
    }

    public List<Photo> getPhotoList() {
        return photoList;
    }

    public int getIndex() {
        return index;
    }

    public void initPhotoList(int albumId){

        photoList = PhotoRepository.getPhotosByAlbumId(albumId);
        setImageList(photoList);

    }

    public void setImageList(List<Photo> photoList){
        for (Photo photo : photoList){
            imageList.add(ByteConverter.byteToImage(photo.getPhoto()));
        }
    }

    public Image getFirstImage(){
        return imageList.get(index);
    }

   public Image next(){
        if (imageList.size()-1>index){
            System.out.println(index);
            index++;
            System.out.println(index);
            return imageList.get(index);


        }else{
            index = 0;
            return imageList.get(index);
        }


    }

    public Image previous(){
        if (imageList.size()-1<index){
            index--;
            return imageList.get(index);
        }else return imageList.get(index);

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
