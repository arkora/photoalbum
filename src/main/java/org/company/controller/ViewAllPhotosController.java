package org.company.controller;

import javafx.scene.image.Image;
import org.company.model.Photo;
import org.company.repository.PhotoRepository;
import org.company.service.ByteConverter;

import java.util.Collections;
import java.util.List;


public class ViewAllPhotosController {

    public List<Photo> loadPhotos(){
        return PhotoRepository.getAllPhotos();
    }

    public Image getAll(int photoid){
        List<Photo> photoList = Collections.singletonList(PhotoRepository.getPhotoById(photoid));
        return ByteConverter.byteToImage(photoList.get(0).getPhoto());
    }
}

