package org.company.controller;

import com.dlsc.gmapsfx.GoogleMapView;
import com.dlsc.gmapsfx.javascript.object.*;
import org.company.model.Photo;
import org.company.service.LatLongConverter;

import java.util.ArrayList;
import java.util.List;

public class MapController {

    private  List<Photo> photoList;

    public void setPhotoList(List<Photo> photoList) {
        this.photoList = photoList;
    }

    private List<Marker> markerList;



    public void configureMap(GoogleMap map, GoogleMapView googleMapView) {

        setMarkerList(LatLongConverter.toLatLong(photoList));



        MapOptions mapOptions = new MapOptions();

        mapOptions.center(new LatLong(0, 0))
                .mapType(MapTypeIdEnum.HYBRID)
                .zoom(0);
        map = googleMapView.createMap(mapOptions, false);

        for (Marker marker : markerList){
            map.addMarker(marker);
        }

    }

    public void setMarkerList(List<LatLong> latLongList){
         markerList = new ArrayList<>();
        MarkerOptions markerOptions = new MarkerOptions();

        for (LatLong latLong : latLongList){
            markerOptions.position(latLong);
            markerList.add(new Marker(markerOptions));
        }


    }
}
