package org.company.controller;

import com.drew.imaging.ImageProcessingException;
import org.company.metadata.PhotoMetadata;
import org.company.model.Photo;
import org.company.repository.AlbumRepository;
import org.company.repository.PhotoRepository;
import org.company.service.PhotoFileService;

import java.io.File;
import java.io.IOException;

public class PhotoController {
   private Photo photo;
   private PhotoMetadata metadata;

    public Photo getPhoto() {
        return photo;
    }

    public PhotoMetadata getMetadata() {
        return metadata;
    }

    public void openPhoto(File file) throws IOException, ImageProcessingException {
        photo = PhotoFileService.getPhotoFromFile(file);
        metadata = photo.getMetadata();
    }


    public void savePhoto(String name) throws NullPointerException{
        Photo photo = new Photo(name,metadata.getSize(), getPhoto().getPhoto(), metadata.getLatitude(),metadata.getLongitude(), AlbumRepository.getLastInsertedAlbumId().intValue());
        PhotoRepository.savePhoto(photo);
    }

}
